# RandomImage

# Contribution

## Code of Conduct

Do not hesitate to contribute.

Please read and follow our [Code of Conduct](CODE_OF_CONDUCT.md).

## How contribute

You can to contribute ? it's easy. 
If you see an issue you are interested to work, please let a message and i assign you the task. With this, all people know you are working on this issue
If it's a new subject, don't hesitate to create an issue. I will check this one to affect some labels and let contribution open 😉

You can initialize a draft merge request directly when you start to work on an issue.

When you finish, past the merge request in "ready" mode and i will check this quickly.

## Execution

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.7.

You can find several npm commands :

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).


# 📖 License

This project is under [the Apache License 2.0](LICENSE)

