import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { PartieModel } from '../models/partie.model';
import { Store } from '@ngrx/store';
import { listeImages } from '../state/jeu.selectors';
import { mettreAJourImage, resetListeImage } from '../state/jeu.action';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PartieService {

  constructor(private store: Store) {
  }

  private _partie: PartieModel;

  private readonly _partieSubject = new Subject<PartieModel>();

  public selectionnerImageRandomDepuisListe(): void {
    this.store.select(listeImages).pipe(take(1)).subscribe((listeImage) => {
      if (listeImage.length === 0) {
        this.store.dispatch(resetListeImage());
        return;
      }

      this.store.dispatch(
          mettreAJourImage({
            imageEnCours: listeImage[Math.floor(Math.random() * listeImage.length)]
          })
      );
    })
  }

  get partie(): PartieModel {
    return this._partie
  }

  set partie(val: PartieModel) {
    this._partie = val;
    this._partieSubject.next(val);
  }

  set joueurs(nombreJoueurs: number) {
    this.partie = {
      nombreJoueurs,
    };
  }
}
