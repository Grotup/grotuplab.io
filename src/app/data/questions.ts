export const questions = [
  {
    titre: 'Privilèges',
    livret: 'assets/livrets/privileges.zip',
    questions: [
      {
        question: 'Qui SUBIRA le moins de critique ?',
        reponse: "C'est le garçon qui subira le moins de critiques",
        reponseAffichee: false,
      },
      {
        question: "Aujourd'hui, qui GAGNE le plus dans notre societé",
        reponse: 'Ce sont les hommes qui gagnent le plus dans notre societé',
        reponseAffichee: false,
      },
      {
        question: "Qui est le plus À L'AISE pour se promener dans la rue ?",
        reponse: "C'est l'homme qui est le plus à l'aise",
        reponseAffichee: false,
      },
      {
        question:
          "Qui sont les plus À L'AISE pour se tenir la main dans la rue",
        reponse: "C'est le couple hétérosexuel qui sera le plus à l'aise",
        reponseAffichee: false,
      },
      {
        question: 'Les enfants trouvent-ils tous des JEUX à leur image ?',
        reponse: 'Non.',
        reponseAffichee: false,
      },
    ],
  },
  {
    titre: 'Stéréotypes',
    livret: 'assets/livrets/stereotypes.zip',
    questions: [
      {
        question:
          'Quelle est la différence de SALAIRE, en %, entre les femmes et les hommes pour un même travail ?',
        reponse: 'En moyenne, les femmes gagnent 23% de moins que les hommes',
        reponseAffichee: false,
      },
      {
        question: 'Les garçons pleurent-t-ils MOINS que les filles ?',
        reponse:
          "Non, mais notre societé accepte plus facile les pleurs d'une filles que ceux d'un garçon",
        reponseAffichee: false,
      },
      {
        question:
          'Quels CLICHÉS entend-on encore sur les femmes et les hommes ?',
        reponse:
          "Les filles font mieux le ménage. Les hommes sont infidèles. Les femmes ne savent pas conduire. Les hommes ne savent faire qu'une seule chose à la fois.",
        reponseAffichee: false,
      },
      {
        question:
          "L'image de la femme est-elle encore associée à celle du MÉNAGE aujourd'hui ?",
        reponse: 'Oui, mais nous constatons que cela évolue',
        reponseAffichee: false,
      },
      {
        question: 'LES POILS des filles et des garçons sont-ils différents',
        reponse:
          'Les hommes et les femmes ont le même taux de pilosité, cependant celui des homme est plus développé en apparence',
        reponseAffichee: false,
      },
      {
        question:
          'Comment les injonctions sociétales influencent notre vision de la vie sexuelle ?',
        reponse:
          'Elles nous influencent dans notre façon de penser et dans notre jugement à l’égard des autres et de nous-même. On retrouve notamment des différences entre les genres.',
        reponseAffichee: false,
      },
    ],
  },
  {
    titre: 'Tabous',
    livret: 'assets/livrets/tabous.zip',
    questions: [
      {
        question: 'LA DURÉE est-elle importante ?',
        reponse: 'Cela dépend de chaque individu',
        reponseAffichee: false,
      },
      {
        question: 'LES RÈGLES sont-elles sales ?',
        reponse: 'Non, elles sont naturelles',
        reponseAffichee: false,
      },
      {
        question: "L'ÉPILATION est-elle importante dans notre societé ?",
        reponse: 'Oui, il y a encore de nombreux clichés autour des poils',
        reponseAffichee: false,
      },
      {
        question: "Qu'est ce que la PORNOGRAPHIE ?",
        reponse:
          "C'est une représentation explicite d'actes sexuels finalisés ayant pour but de suciter de l'excitation sexuelle",
        reponseAffichee: false,
      },
      {
        question: 'Y a-t-il UNE TAILLE réglementaire ?',
        reponse: 'Non, cela dépend de chaque individu',
        reponseAffichee: false,
      },
    ],
  },
  {
    titre: 'Violences',
    livret: 'assets/livrets/violences.zip',
    questions: [
      {
        question:
          'Combien de femmes ont été TUÉES par leur partenaire ou leur ex-partenaire en 2019 ?',
        reponse:
          'En 2019, il y a 146 femmes et 27 hommes qui ont été tués par leur partenaire ou ex-partenaire. 25 enfants mineurs ont été tués par un de leurs parents dans un contexte de violence au seins du couple',
        reponseAffichee: false,
      },
      {
        question:
          'Combien de femmes ont été victimes de HARCÈLEMENT SEXUEL dans les lieux publics en France en 2019 ?',
        reponse:
          '81% des femmes disent être victimes de harcèlement sexuel. En 25% des hommes ont subi la même chose, principalement des homosexuels',
        reponseAffichee: false,
      },
      {
        question: "Qu'est ce que LE HARCÈLEMENT SCOLAIRE ?",
        reponse:
          'Propos ou comportement répété ayant pour object ou pour effet une dégradation de ses conditions de vie, se traduisant par une altération de sa santé physique ou mentale',
        reponseAffichee: false,
      },
      {
        question: "Qu'est ce que LE SLUT-SHAMING ?",
        reponse:
          "Le slut-shaming regroupe un ensemble d'attitude individuelles ou collectives, agressives envers les femmes, dont le comportement sexuel serait jugé hors normes",
        reponseAffichee: false,
      },
      {
        question:
          'Quelles sont les différentes formes de VIOLENCES CONJUGALES ?',
        reponse:
          'Violences physiques, violences sexuelles, violences psychologiques, violences économiques, violences administratives',
        reponseAffichee: false,
      },
      {
        question: 'Peut-on toucher sans demander ?',
        reponse:
          'La réponse est bien entendu non mais pour aller plus loin, avez-vous identifié le terme qui parle de cette situation ? Il s’agit du consentement.',
        reponseAffichee: false,
      },
      {
        question: 'Quelle attitude adopter dans cette situation ?',
        reponse:
          'Cette situation décrit du «upskirtting», qui signifie «sous la jupe» et se caractérise par des photographies prises sous la jupe/robe de femmes. Plusieurs actions sont possibles (voir Annexes) «Si vous vous sentez en sécurité, intervenez pour faire cesser cette situation dont vous êtes témoin. Vous pouvez adopter plusieurs stratégies : mobilisation des autres témoins, confrontation avec l’agresseur ou le harceleur, diversion… Votre réaction doit être proportionnelle à la menace.',
        reponseAffichee: false,
      },
      {
        question:
          'Quel est le pourcentage de vidéos pornos qui contiennent des violences ?',
        reponse:
          '90 % des contenus porno présentent des actes non simulés de violences physiques, sexuelles ou verbales envers les femmes, selon une étude menée en 2023 par le Haut Conseil de l’Égalité.',
        reponseAffichee: false,
      },
    ],
  },
];
