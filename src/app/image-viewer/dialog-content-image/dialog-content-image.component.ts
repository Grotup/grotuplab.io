import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-dialog-content-image',
    templateUrl: 'dialog-content-image.component.html',
    styleUrls: ['./dialog-content-image.component.scss']
})
export class DialogContentImageComponent {
    constructor(@Inject(MAT_DIALOG_DATA) public imgUrl: string) {
        console.log(imgUrl);
    }
}
